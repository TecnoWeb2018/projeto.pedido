using Microsoft.EntityFrameworkCore;
using projeto.pedido.DbContext;
using projeto.pedido.Models.Entities;
using projeto.pedido.Repository.Base;

namespace projeto.pedido.Repository
{
    public class ClientRepository : BaseRepository<DbMContext, ClientEntity>
    {
        public ClientRepository(DbMContext context) : base(context, context.Clients)
        {
        }
    }
}