using Microsoft.EntityFrameworkCore;
using projeto.pedido.DbContext;
using projeto.pedido.Models.Entities;
using projeto.pedido.Repository.Base;

namespace projeto.pedido.Repository
{
    public class ProductRepository : BaseRepository<DbMContext, ProductEntity>
    {
        public ProductRepository(DbMContext context) : base(context, context.Products)
        {
        }
    }
}