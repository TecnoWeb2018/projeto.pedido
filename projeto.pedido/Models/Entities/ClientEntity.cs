using projeto.pedido.Models.Entities.Base;

namespace projeto.pedido.Models.Entities
{
    public class ClientEntity : BaseEntity
    {
        public string Name { get; set; }

        public string Phone { get; set; }

        public override string ToString()
        {
            var s = base.ToString();
            s += Name + " ";
            s += Phone + " ";
            return s;
        }
    }
}