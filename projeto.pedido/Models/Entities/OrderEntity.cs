using System;
using System.ComponentModel.DataAnnotations.Schema;
using projeto.pedido.Extensions;
using projeto.pedido.Models.Entities.Base;

namespace projeto.pedido.Models.Entities
{
    public class OrderEntity : BaseEntity
    {
        public int ClientId { get; set; }
        public int ProductId { get; set; }
        public double TotalValue { get; set; }
        
        [ForeignKey("ClientId")]
        public ClientEntity Client { get; set; }

        [ForeignKey("ProductId")]
        public ProductEntity Product { get; set; }
        
        public override string ToString()
        {
            var s = base.ToString();
            s += Client == null ? "" : Client.ToString();
            s += Product == null ? "" : Product.ToString();
            s += TotalValue.FormatToCurrency() + " ";
            return s;
        }
        
    }
}